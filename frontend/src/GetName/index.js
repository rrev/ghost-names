import React, { Component, Fragment } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import GetNameForm from '~/src/GetNameForm';
import GhostNameSelector from '~/src/GhostNameSelector';

import './index.css';

@inject('ghosts')
@inject('user')
@observer
export default class GetName extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        firstName: '',
        lastName: '',
      },
    };

    this.onFormChange = this.onFormChange.bind(this);

    this.onFetchUnusedNames = this.onFetchUnusedNames.bind(this);
    this.onAssignName = this.onAssignName.bind(this);
  }

  componentWillMount() {
    this.inizializeFormToUserGhostName();
  }

  componentWillReact() {
    if (this.props.user.isLogged && this.isFormEmpty()) {
      this.inizializeFormToUserGhostName();
    }
  }

  inizializeFormToUserGhostName() {
    const { ghost } = this.props.user;

    if (ghost) {
      this.setState({
        form: {
          firstName: ghost.firstName,
          lastName: ghost.lastName,
        },
      });
    }
  }

  onFetchUnusedNames(e) {
    e.preventDefault();

    this.props.ghosts.fetchUnusedNames();
  }

  onFormChange(form) {
    this.setState({ form: form });
  }

  onAssignName(choosenName) {
    const { firstName, lastName } = this.state.form;

    if (this.props.user.assignGhostNameStatus.loading) {
      return;
    }

    this.props.user.assignGhostNameToUser({
      key: choosenName,
      firstName,
      lastName,
    });
  }

  isFormEmpty() {
    return (
      this.state.form.firstName.length === 0 &&
      this.state.form.lastName.length === 0
    );
  }

  render() {
    const { form } = this.state;
    const {
      isLogged,
      loginUrl,
      ghostName,
      assignGhostNameStatus,
    } = this.props.user;

    const showNames = !this.isFormEmpty();

    if (!isLogged) {
      return (
        <Fragment>
          <p>Log in to continue!</p>
          {!loginUrl && <p>Wait while we get the login url</p>}
          {loginUrl && <a href={loginUrl}>Click here to login</a>}
        </Fragment>
      );
    }

    return (
      <div className="get-name">
        <h1>
          {!ghostName && `Get a 👻 name!`}
          {ghostName && `Change your 👻 name!`}
        </h1>

        <GetNameForm
          form={form}
          onSubmit={this.onFetchUnusedNames}
          onFormChange={this.onFormChange}
        />

        {showNames && (
          <GhostNameSelector onSubmit={this.onAssignName} form={form} />
        )}

        {!assignGhostNameStatus.loading &&
          assignGhostNameStatus.errorMessage && (
            <p>
              Ops! Something went wrong: {assignGhostNameStatus.errorMessage}
            </p>
          )}

        {!assignGhostNameStatus.loading && assignGhostNameStatus.success && (
          <p>
            Nice! Your name is now: {ghostName}.{' '}
            <Link to="/">Go back to the overview</Link> or change it again!
          </p>
        )}
      </div>
    );
  }
}
