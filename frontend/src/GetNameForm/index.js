import React, { Component } from 'react';

export default class GetNameForm extends Component {
  constructor(props) {
    super(props);

    this.onChangeFirstName = this.onChange.bind(this, 'firstName');
    this.onChangeLastName = this.onChange.bind(this, 'lastName');
  }

  onChange(what, e) {
    const form = this.props.form;
    this.props.onFormChange({
      ...form,
      [what]: e.target.value,
    });
  }

  render() {
    const { onSubmit, form } = this.props;

    return (
      <form onSubmit={onSubmit}>
        <label>
          First name
          <input
            type="text"
            placeholder="Please insert here your first name"
            onChange={this.onChangeFirstName}
            value={form.firstName}
          />
        </label>
        <label>
          Last name
          <input
            type="text"
            placeholder="Please insert here your last name"
            onChange={this.onChangeLastName}
            value={form.lastName}
          />
        </label>
        <input
          disabled={!form.lastName || !form.firstName}
          type="submit"
          value="Get names!"
        />
      </form>
    );
  }
}
