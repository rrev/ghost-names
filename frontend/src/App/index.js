import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Route } from 'react-router-dom';

import { inject, observer } from 'mobx-react';

import Home from '~/src/Home';
import Header from '~/src/Header';
import GetName from '~/src/GetName';

import './base.css';

@inject('user')
@withRouter
@observer
export default class App extends Component {
  componentWillMount() {
    this.props.user.fetchUser();
  }

  render() {
    return (
      <div>
        <Header />
        <Route exact path="/" component={Home} />
        <Route exact path="/get-name" component={GetName} />
      </div>
    );
  }
}
