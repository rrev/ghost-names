import { apiUrl } from '~/src/Config';
import { getFetch, mapRemoteGhostToLocal } from '~/src/Utils';

import { action, observable } from 'mobx';

class Ghosts {
  @observable randomizedGhosts = {
    loading: false,
    ghosts: [],
    errorMessage: '',
  };

  @observable unusedNames = {
    loading: false,
    success: false,
    list: [],
    errorMessage: '',
  };

  @action
  async fetchUnusedNames() {
    this.unusedNames = {
      loading: true,
      success: false,
      list: [],
      errorMessage: '',
    };

    try {
      const response = await getFetch(
        `${apiUrl}ghosts/?unassigned=true&count=3`
      );

      if (response.status !== 200) {
        throw new Error('Unable to fetch unused names');
      }

      const ghosts = await response.json();

      this.unusedNames = {
        loading: false,
        success: true,
        list: ghosts.map(mapRemoteGhostToLocal),
        errorMessage: '',
      };
    } catch (e) {
      this.unusedNames = {
        loading: false,
        success: false,
        list: [],
        errorMessage:
          'Unable to get the list of available names, please check your connection!',
      };
    }
  }

  @action
  async fetchRandomGhosts() {
    this.randomizedGhosts = {
      loading: true,
      ghosts: [],
      errorMessage: '',
    };

    try {
      const response = await getFetch(`${apiUrl}ghosts/`);

      if (response.status !== 200) {
        throw new Error('Unable to fetch random ghosts');
      }

      const ghosts = await response.json();
      this.randomizedGhosts = {
        loading: false,
        ghosts: ghosts.map(mapRemoteGhostToLocal),
        errorMessage: '',
      };
    } catch (e) {
      this.randomizedGhosts = {
        loading: false,
        ghosts: [],
        errorMessage:
          'Unable to fetch the ghosts, check yuor internet connection',
      };
    }
  }
}

export default new Ghosts();
