import { apiUrl } from '~/src/Config';
import { getFetch, putFetch, HttpError } from '~/src/Utils';

import { action, observable, computed } from 'mobx';

class User {
  @observable isLogged = false;

  @observable loginUrl = null;
  @observable logoutUrl = null;
  @observable id = null;
  @observable email = null;
  @observable ghost = null;

  @observable assignGhostNameStatus = {
    loading: false,
    success: false,
    errorMessage: '',
  };

  @computed get ghostName() {
    if (!this.ghost) {
      return '';
    }

    const ghost = this.ghost;
    return `${ghost.firstName} "${ghost.name}" ${ghost.lastName}`;
  }

  @computed get hasGhostName() {
    return this.ghost !== null;
  }

  @action
  async assignGhostNameToUser({ key, firstName, lastName }) {
    this.assignGhostNameStatus = {
      loading: true,
      success: false,
      errorMessage: '',
    };

    try {
      const response = await putFetch(`${apiUrl}ghosts/${key}/`, {
        body: JSON.stringify({
          first_name: firstName,
          last_name: lastName,
        }),
      });

      if (response.status === 400) {
        throw new HttpError(response);
      }

      const json = await response.json();

      this.ghost = {
        name: json.name,
        firstName: json.first_name,
        lastName: json.last_name,
      };

      this.assignGhostNameStatus = {
        loading: false,
        success: true,
        errorMessage: '',
      };
    } catch (e) {
      console.error('unable to assign the name to the user', e);

      let message = 'Unable to assign the name to you :( Try again later';

      if (
        e instanceof HttpError &&
        e.response.status >= 400 &&
        e.response.status <= 500
      ) {
        const errorJson = await e.response.json();
        message = errorJson.message;
      }

      this.assignGhostNameStatus = {
        loading: false,
        success: false,
        errorMessage: message,
      };
    }
  }

  @action
  async fetchUser() {
    try {
      const me = await getFetch(`${apiUrl}me/`);

      const json = await me.json();

      if (me.status === 401) {
        this.loginUrl = json.login_url;
        return;
      }

      this.isLogged = true;

      this.id = json.id;
      this.logoutUrl = json.logout_url;
      this.email = json.email;

      if (json.ghost) {
        this.ghost = {
          firstName: json.ghost.first_name,
          lastName: json.ghost.last_name,
          name: json.ghost.name,
        };
      }
    } catch (e) {
      console.error('Unable to fetch user', e);
    }
  }
}

export default new User();
