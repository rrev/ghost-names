import React, { Component, Fragment } from 'react';
import { inject, observer } from 'mobx-react';

@inject('ghosts')
@observer
export default class GhostsList extends Component {
  componentWillMount() {
    if (this.props.ghosts.randomizedGhosts.ghosts.length === 0) {
      this.props.ghosts.fetchRandomGhosts();
    }
  }

  render() {
    const {
      ghosts: { randomizedGhosts },
    } = this.props;

    return (
      <Fragment>
        {!randomizedGhosts.loading && randomizedGhosts.errorMessage && (
          <p>{randomizedGhosts.errorMessage}</p>
        )}

        {randomizedGhosts.loading && <p>Loading!</p>}

        <ul>
          {randomizedGhosts.ghosts.map(ghost => (
            <li key={ghost.key}>
              <h2>{ghost.name}</h2>
              {ghost.takenById && (
                <Fragment>
                  <p>{ghost.description}</p>
                  <p>
                    ☹️ Taken! by {ghost.takenByFirstName} "{ghost.name}"{' '}
                    {ghost.takenByLastName} - ({ghost.takenByEmail})
                  </p>
                </Fragment>
              )}
              {!ghost.takenById && (
                <Fragment>
                  <p>{ghost.description}</p>
                  <p>✨ Available!</p>
                </Fragment>
              )}
            </li>
          ))}
        </ul>
      </Fragment>
    );
  }
}
