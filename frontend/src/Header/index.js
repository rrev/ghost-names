import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Header extends Component {
  render() {
    const { user } = this.props;

    return (
      <ul>
        <li>
          <Link to="/">Overview</Link>
        </li>
        {!user.isLogged && (
          <li>
            <a href={user.loginUrl}>Login</a>
          </li>
        )}
        {user.isLogged && (
          <li>
            <a href={user.logoutUrl}>Logout</a>
          </li>
        )}
        {!user.hasGhostName && (
          <li>
            <Link to="/get-name">Get a Phantom name!</Link>
          </li>
        )}
        {user.hasGhostName && (
          <li>
            <Link to="/get-name">
              Your name is {user.ghostName}. Click here to change!
            </Link>
          </li>
        )}
      </ul>
    );
  }
}
