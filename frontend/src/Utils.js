export const jsonFetch = (url, options = {}) => {
  return fetch(url, buildFetchOptions(options));
};

export const getFetch = (url, options = {}) => {
  options = buildFetchOptions(options);
  options.method = 'GET';
  return fetch(url, options);
};

export const putFetch = (url, options = {}) => {
  options = buildFetchOptions(options);
  options.method = 'PUT';
  return fetch(url, options);
};

const buildFetchOptions = options => {
  options.headers = options.headers || {};
  options.credentials = 'include';

  if (!options.headers.hasOwnProperty('Content-Type')) {
    options.headers['Content-Type'] = 'application/json';
  }

  if (!options.headers.hasOwnProperty('Accept')) {
    options.headers['Accept'] = 'application/json';
  }

  return options;
};

export const mapRemoteGhostToLocal = remote => ({
  name: remote.name,
  key: remote.key,
  takenByFirstName: remote.taken_by_first_name,
  takenById: remote.taken_by_id,
  takenByLastName: remote.taken_by_last_name,
  takenByEmail: remote.taken_by_email,
  description: remote.description,
});

export class HttpError extends Error {
  constructor(response) {
    super(`${response.url} (${response.status})`);
    this.name = 'HttpError';
    this.response = response;
  }
}
