import React, { Component, Fragment } from 'react';
import { inject, observer } from 'mobx-react';

@inject('ghosts')
@inject('user')
@observer
export default class GhostNameSelector extends Component {
  state = {
    choosenName: '',
  };

  constructor(props) {
    super(props);

    this.onChangeChoosenName = this.onChangeChoosenName.bind(this);
    this.onAssignName = this.onAssignName.bind(this);
  }

  onChangeChoosenName(e) {
    this.setState({ choosenName: e.target.value });
  }

  onAssignName(e) {
    e.preventDefault();
    this.props.onSubmit(this.state.choosenName);
  }

  render() {
    const { choosenName } = this.state;

    const {
      form,
      user: { assignGhostNameStatus },
      ghosts: { unusedNames },
    } = this.props;

    return (
      <Fragment>
        <h2>Choose your name!</h2>
        <p>
          Click `Get names`{' '}
          {unusedNames.list.length > 0 && `again to get more!`}
          {unusedNames.list.length === 0 && `to see them!`}
        </p>

        {!unusedNames.loading && unusedNames.errorMessage && (
          <p>{unusedNames.errorMessage}</p>
        )}

        {!unusedNames.loading &&
          unusedNames.success &&
          unusedNames.list.length === 0 && (
            <p>Looks like we run out of names :(</p>
          )}
        {unusedNames.list.length > 0 && (
          <Fragment>
            <form onSubmit={this.onAssignName}>
              <ul>
                {unusedNames.list.map(ghost => (
                  <li key={ghost.key}>
                    <label>
                      <input
                        checked={choosenName === ghost.key}
                        onChange={this.onChangeChoosenName}
                        type="radio"
                        name="name"
                        value={ghost.key}
                      />{' '}
                      {form.firstName} "{ghost.name}" {form.lastName}
                    </label>
                  </li>
                ))}
              </ul>
              <input
                disabled={choosenName === null || assignGhostNameStatus.loading}
                type="submit"
                value={
                  assignGhostNameStatus.loading ? 'Please wait' : 'Get it!'
                }
              />
            </form>
          </Fragment>
        )}
      </Fragment>
    );
  }
}
