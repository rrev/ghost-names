import React, { Component } from 'react';
import { withRouter } from 'react-router';

import GhostsList from '~/src/GhostsList';

import './index.css';

class Home extends Component {
  render() {
    return (
      <div className="home">
        <h1>👻 names!</h1>
        <GhostsList />
      </div>
    );
  }
}

export default withRouter(Home);
