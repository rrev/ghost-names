import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import { Provider } from 'mobx-react';

import user from '~/src/Stores/user';
import ghosts from '~/src/Stores/ghosts';

import App from '~/src/App';

ReactDOM.render(
  <Provider user={user} ghosts={ghosts}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.querySelector('.app')
);
