import webapp2
import main
import json

from google.appengine.api import users

from .base import BaseTestCase
from .factories import GhostFactory


class UserTestCase(BaseTestCase):
    def testGetUserInfoAsUnlogged(self):
        self.assertFalse(users.get_current_user())

        request = webapp2.Request.blank('/api/me/')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 401)

        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'Not authenticated',
            'login_url': users.create_login_url()
        })

    def testGetUserInfo(self):
        self.loginUser()

        request = webapp2.Request.blank('/api/me/')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)

        self.assertEqual(body, {
            'ghost': None,
            'id': '123',
            'logout_url': users.create_logout_url('/'),
            'email': 'user@email.it'
        })

    def testGetUserInfoWithGhostName(self):
        self.loginUser()

        ghost = GhostFactory(assigned=True, taken_by_id='123')

        request = webapp2.Request.blank('/api/me/')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)

        self.assertEqual(body, {
            'ghost': {
                'first_name': ghost.taken_by_first_name,
                'last_name': ghost.taken_by_last_name,
                'name': ghost.name,
            },
            'id': '123',
            'logout_url': users.create_logout_url('/'),
            'email': 'user@email.it'
        })
