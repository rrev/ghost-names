import webapp2
import main
import json

from mock import patch

from google.appengine.api import users

from .base import BaseTestCase
from .factories import GhostFactory


class GhostsTestCase(BaseTestCase):
    @patch('backend.models.random.randint')
    def testGhostsAreRandomlyOrderedUsingTheRandomSeed(self, mock_random):
        mock_random.return_value = 0

        ghosts = [
            GhostFactory(random_seed=0),
            GhostFactory(random_seed=1),
            GhostFactory(random_seed=2, assigned=True),
            GhostFactory(random_seed=3),
            GhostFactory(random_seed=4)
        ]

        request = webapp2.Request.blank('/api/ghosts/')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)
        count_ghosts = len(ghosts)

        self.assertEqual(count_ghosts, 5)

        for i in range(0, count_ghosts):
            ghost = ghosts[i]

            self.assertEqual(body[i], {
                'random_seed': ghost.random_seed,
                'name': ghost.name,
                'taken_by_id': ghost.taken_by_id,
                'taken_by_email': ghost.taken_by_email,
                'taken_by_first_name': ghost.taken_by_first_name,
                'taken_by_last_name': ghost.taken_by_last_name,
                'description': ghost.description,
                'key': ghost.key.urlsafe(),
            })

    @patch('backend.models.random.randint')
    def testLimitGhostsResults(self, mock_random):
        mock_random.return_value = 0

        ghosts = [
            GhostFactory(random_seed=0),
            GhostFactory(random_seed=1),
            GhostFactory(random_seed=2, assigned=True),
            GhostFactory(random_seed=3),
            GhostFactory(random_seed=4)
        ]

        request = webapp2.Request.blank('/api/ghosts/?count=2')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)

        self.assertEqual(len(body), 2)

        for i in range(0, 2):
            ghost = ghosts[i]

            self.assertEqual(body[i], {
                'random_seed': ghost.random_seed,
                'name': ghost.name,
                'taken_by_id': ghost.taken_by_id,
                'taken_by_email': ghost.taken_by_email,
                'taken_by_first_name': ghost.taken_by_first_name,
                'taken_by_last_name': ghost.taken_by_last_name,
                'description': ghost.description,
                'key': ghost.key.urlsafe(),
            })

    @patch('backend.models.random.randint')
    def testNegativeGhostsCountValue(self, mock_random):
        mock_random.return_value = 0

        ghosts = [
            GhostFactory(random_seed=0),
            GhostFactory(random_seed=1),
            GhostFactory(random_seed=2, assigned=True),
            GhostFactory(random_seed=3),
            GhostFactory(random_seed=4)
        ]

        request = webapp2.Request.blank('/api/ghosts/?count=-5')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)

        self.assertEqual(len(body), 1)

        ghost = ghosts[0]

        self.assertEqual(body[0], {
            'random_seed': ghost.random_seed,
            'name': ghost.name,
            'taken_by_id': ghost.taken_by_id,
            'taken_by_email': ghost.taken_by_email,
            'taken_by_first_name': ghost.taken_by_first_name,
            'taken_by_last_name': ghost.taken_by_last_name,
            'description': ghost.description,
            'key': ghost.key.urlsafe(),
        })

    @patch('backend.models.random.randint')
    def testGhostsCountValueCannotBeHighterThan50(self, mock_random):
        mock_random.return_value = 0

        for i in range(0, 80):
            GhostFactory(random_seed=i)

        request = webapp2.Request.blank('/api/ghosts/?count=100')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)

        self.assertEqual(len(body), 50)

    @patch('backend.models.random.randint')
    def testFilterGetNotAssignedGhosts(self, mock_random):
        mock_random.return_value = 0

        unassigned1 = GhostFactory(random_seed=0)
        GhostFactory(random_seed=1, assigned=True)
        GhostFactory(random_seed=2, assigned=True)
        unassigned2 = GhostFactory(random_seed=3)
        GhostFactory(random_seed=4, assigned=True)

        request = webapp2.Request.blank('/api/ghosts/?unassigned=true')
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)

        body = json.loads(response.body)

        self.assertEqual(len(body), 2)

        self.assertItemsEqual(
            [
                {
                    'random_seed': unassigned1.random_seed,
                    'name': unassigned1.name,
                    'taken_by_id': unassigned1.taken_by_id,
                    'taken_by_email': unassigned1.taken_by_email,
                    'taken_by_first_name': unassigned1.taken_by_first_name,
                    'taken_by_last_name': unassigned1.taken_by_last_name,
                    'description': unassigned1.description,
                    'key': unassigned1.key.urlsafe(),
                },
                {
                    'random_seed': unassigned2.random_seed,
                    'name': unassigned2.name,
                    'taken_by_id': unassigned2.taken_by_id,
                    'taken_by_email': unassigned2.taken_by_email,
                    'taken_by_first_name': unassigned2.taken_by_first_name,
                    'taken_by_last_name': unassigned2.taken_by_last_name,
                    'description': unassigned2.description,
                    'key': unassigned2.key.urlsafe(),
                }
            ],
            body
        )


class AssignGhostTestCase(BaseTestCase):
    def testCannotAssignNameIfUnlogged(self):
        ghost_key = GhostFactory().key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 401)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'Not authenticated',
            'login_url': users.create_login_url()
        })

    def testRandomGhostKeyReturns404(self):
        self.loginUser()

        request = webapp2.Request.blank('/api/ghosts/random_key/', POST=json.dumps({
            'first_name': 'Test First',
            'last_name': 'Test Last',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 404)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'No ghost found with this key'
        })

    def testAssignNameToLoggedUser(self):
        self.loginUser()

        ghost = GhostFactory()
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': 'Test First',
            'last_name': 'Test Last',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'first_name': 'Test First',
            'last_name': 'Test Last',
            'name': ghost.name,
        })

    def testChangeAssignedName(self):
        self.loginUser()

        ghost1 = GhostFactory()
        ghost1_key = ghost1.key.urlsafe()

        ghost2 = GhostFactory(assigned=True, taken_by_id='123')
        ghost2_key = ghost2.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost1_key), POST=json.dumps({
            'first_name': 'Neww',
            'last_name': 'Name',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)
        body = json.loads(response.body)

        ghost1 = ghost1.key.get()
        ghost2 = ghost2.key.get()

        self.assertEqual(ghost1.taken_by_id, '123')
        self.assertEqual(ghost1.taken_by_first_name, 'Neww')
        self.assertEqual(ghost1.taken_by_last_name, 'Name')
        self.assertEqual(ghost1.taken_by_email, 'user@email.it')

        self.assertEqual(ghost2.taken_by_id, '')
        self.assertEqual(ghost2.taken_by_first_name, '')
        self.assertEqual(ghost2.taken_by_last_name, '')
        self.assertEqual(ghost2.taken_by_email, '')

        self.assertEqual(body, {
            'first_name': 'Neww',
            'last_name': 'Name',
            'name': ghost1.name,
        })

    def testCanAssignSameNameToTheAlreadyAssignedUser(self):
        self.loginUser()

        ghost = GhostFactory(assigned=True, taken_by_id='123')
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': 'Test First',
            'last_name': 'Test Last',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'first_name': 'Test First',
            'last_name': 'Test Last',
            'name': ghost.name,
        })

    def testTryingToAssignNameToAlreadyAssignedUserUpdatesFirstLastNameAndEmail(self):
        self.loginUser()

        ghost = GhostFactory(assigned=True, taken_by_id='123')
        ghost_key = ghost.key.urlsafe()

        self.assertNotEqual(ghost.taken_by_first_name, 'New Test')
        self.assertNotEqual(ghost.taken_by_last_name, 'Old test')
        self.assertNotEqual(ghost.taken_by_email, 'user@email.it')

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': 'New Test',
            'last_name': 'Old test',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 200)
        body = json.loads(response.body)

        ghost = ghost.key.get()

        self.assertEqual(ghost.taken_by_email, 'user@email.it')

        self.assertEqual(body, {
            'first_name': 'New Test',
            'last_name': 'Old test',
            'name': ghost.name,
        })

    def testCannotAssignSomeoneElseName(self):
        self.loginUser()

        ghost = GhostFactory(assigned=True, taken_by_id='567')
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': 'Test First',
            'last_name': 'Test Last',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 400)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'This name is already taken'
        })

    def testFirstNameParamIsRequired(self):
        self.loginUser()

        ghost = GhostFactory()
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'last_name': 'Test Last',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 400)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'First name is required and cannot be empty'
        })

    def testFirstNameParamCannotBeBlank(self):
        self.loginUser()

        ghost = GhostFactory()
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': '',
            'last_name': 'Test Last',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 400)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'First name is required and cannot be empty'
        })

    def testLastNameParamIsRequired(self):
        self.loginUser()

        ghost = GhostFactory()
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': 'Test First',
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 400)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'Last name is required and cannot be empty'
        })

    def testLastNameParamCannotBeBlank(self):
        self.loginUser()

        ghost = GhostFactory()
        ghost_key = ghost.key.urlsafe()

        request = webapp2.Request.blank('/api/ghosts/{}/'.format(ghost_key), POST=json.dumps({
            'first_name': 'Test First',
            'last_name': ''
        }))
        request.method = 'PUT'
        response = request.get_response(main.app)

        self.assertEqual(response.status_int, 400)
        body = json.loads(response.body)

        self.assertEqual(body, {
            'message': 'Last name is required and cannot be empty'
        })
