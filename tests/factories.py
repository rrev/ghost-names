import factory

from backend.models import Ghost


class GhostFactory(factory.Factory):
    class Meta:
        model = Ghost

    name = factory.Faker('word')
    description = factory.Faker('text')
    random_seed = factory.Faker('pyint')

    taken_by_id = factory.Faker('msisdn')
    taken_by_first_name = factory.Faker('first_name')
    taken_by_last_name = factory.Faker('last_name')
    taken_by_email = factory.Faker('safe_email')

    @classmethod
    def _create(cls, model_class, assigned=False, *args, **kwargs):
        obj = model_class(*args, **kwargs)

        if not assigned:
            obj.taken_by_id = ''
            obj.taken_by_first_name = ''
            obj.taken_by_last_name = ''
            obj.taken_by_email = ''

        obj.put()
        return obj
