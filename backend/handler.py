import os
import webapp2
import json

from google.appengine.api import users


class BaseRequestHandler(webapp2.RequestHandler):
    def dispatch(self):
        if self.request.method != 'OPTIONS':
            self.response.headers.add_header('Content-Type', 'application/json')

        self.response.headers.add_header('Access-Control-Allow-Origin', os.environ.get('EXTERNAL_ORIGINS', ''))
        self.response.headers.add_header('Access-Control-Allow-Headers', 'Content-Type')
        self.response.headers.add_header('Access-Control-Allow-Credentials', 'true')
        self.response.headers.add_header('Access-Control-Allow-Methods', 'GET, PUT')

        webapp2.RequestHandler.dispatch(self)

    def error_response(self, message, status_code=400, extras={}):
        self.response.status = status_code

        payload = {'message': message}
        payload.update(extras)

        error_payload = json.dumps(payload)

        self.response.write(error_payload)

    def success_response(self, payload, status_code=200):
        self.response.status = status_code
        json_payload = json.dumps(payload)
        self.response.write(json_payload)

    def json_body(self):
        if not hasattr(self, '__json_body') or not self.__json_body:
            body = self.request.body
            self.__json_body = json.loads(body)

        return self.__json_body

    def get_current_user(self):
        if not hasattr(self, '__user'):
            self.__user = users.get_current_user()

        return self.__user

    def options(self, *args, **kwargs):
        # Add the CORS headers to the OPTIONS request (for the local development environment)
        pass
