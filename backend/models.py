import random

from google.appengine.ext import ndb


class Ghost(ndb.Model):
    name = ndb.StringProperty(indexed=False)
    description = ndb.TextProperty(indexed=False)
    random_seed = ndb.IntegerProperty(indexed=True)

    taken_by_id = ndb.StringProperty(indexed=True)
    taken_by_first_name = ndb.StringProperty(indexed=False)
    taken_by_last_name = ndb.StringProperty(indexed=False)
    taken_by_email = ndb.StringProperty(indexed=False)

    def to_dict(self, *args, **kwargs):
        as_dict = super(Ghost, self).to_dict(*args, **kwargs)
        as_dict['key'] = self.key.urlsafe()
        return as_dict

    def assign_to(self, user_id, first_name, last_name, email):
        me = ndb.Key(urlsafe=self.key.urlsafe()).get()

        if me.taken_by_id:
            raise ValueError('This name is already taken')

        me.taken_by_id = user_id
        me.taken_by_first_name = first_name
        me.taken_by_last_name = last_name
        me.taken_by_email = email
        me.put()

    def unassign(self):
        self.taken_by_id = ''
        self.taken_by_first_name = ''
        self.taken_by_last_name = ''
        self.taken_by_email = ''
        self.put()

    @classmethod
    def get_random_ghosts(cls, unassigned=False, count=5):
        count = max(min(count, 50), 1)
        number = random.randint(0, 50 - count)

        query = cls.query(cls.random_seed >= number)

        if unassigned:
            query = query.filter(cls.taken_by_id == '')

        return query.fetch(count)
