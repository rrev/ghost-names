# -*- coding: utf-8 -*-

import webapp2

from google.appengine.api import users
from google.appengine.ext import ndb
from google.net.proto.ProtocolBuffer import ProtocolBufferDecodeError

from .models import Ghost
from .utils import get_user_ghost
from .handler import BaseRequestHandler


class MeEndpoint(BaseRequestHandler):
    def get(self):
        user = self.get_current_user()

        if not user:
            self.error_response('Not authenticated', 401, {
                'login_url': users.create_login_url('/')
            })
            return

        user_id = user.user_id()
        ghost = get_user_ghost(user_id)

        payload = {
            'id': user_id,
            'email': user.email(),
            'logout_url': users.create_logout_url('/'),
            'ghost': None
        }

        if ghost:
            payload['ghost'] = {
                'name': ghost.name,
                'first_name': ghost.taken_by_first_name,
                'last_name': ghost.taken_by_last_name,
            }

        self.success_response(payload)


class GhostNamesEndpoint(BaseRequestHandler):
    def get(self):
        count = self.request.GET.get('count', 5)

        try:
            count = int(count)
        except ValueError:
            count = 5

        unassigned = self.request.GET.get('unassigned', 'false') == 'true'

        random_ghosts = Ghost.get_random_ghosts(
            unassigned,
            count,
        )

        response = [ghost.to_dict() for ghost in random_ghosts]
        self.success_response(response)


class GhostNameEndpoint(BaseRequestHandler):
    def validate(self):
        user = self.get_current_user()

        if not user:
            self.error_response('Not authenticated', 401, {
                'login_url': users.create_login_url('/')
            })
            return False

        payload = self.json_body()

        if 'first_name' not in payload or not payload['first_name']:
            self.error_response('First name is required and cannot be empty')
            return False

        if 'last_name' not in payload or not payload['last_name']:
            self.error_response('Last name is required and cannot be empty')
            return False

        return True

    def put(self, key):
        if not self.validate():
            return

        user = self.get_current_user()
        user_ghost = get_user_ghost(user.user_id())
        self.assign_name(key, user_ghost)

    @ndb.transactional(retries=0, xg=True)
    def assign_name(self, key, user_ghost):
        user = self.get_current_user()

        payload = self.json_body()

        first_name = payload['first_name']
        last_name = payload['last_name']

        try:
            ghost = ndb.Key(urlsafe=key).get()
        except ProtocolBufferDecodeError:
            self.error_response('No ghost found with this key', 404)
            return

        if not ghost:
            self.error_response('No ghost found with this key', 404)
            return

        if user_ghost and ghost.key == user_ghost.key:
            ghost.taken_by_first_name = first_name
            ghost.taken_by_last_name = last_name
            ghost.taken_by_email = user.email()
            ghost.put()
        else:
            try:
                ghost.assign_to(
                    user_id=user.user_id(),
                    first_name=first_name,
                    last_name=last_name,
                    email=user.email()
                )

                if user_ghost:
                    user_ghost.unassign()
            except ValueError as e:
                self.error_response(str(e))
                raise ndb.Rollback()

        self.success_response({
            'name': ghost.name,
            'first_name': ghost.taken_by_first_name,
            'last_name': ghost.taken_by_last_name,
        })
