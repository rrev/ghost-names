from .models import Ghost


def get_user_ghost(user_id):
    return Ghost.query(Ghost.taken_by_id == user_id).get()
