from webapp2 import WSGIApplication, Route
from webapp2_extras.routes import PathPrefixRoute

from backend.views import MeEndpoint, GhostNamesEndpoint, GhostNameEndpoint


app = WSGIApplication([
    PathPrefixRoute('/api', [
        Route('/me/', MeEndpoint),
        Route('/ghosts/', GhostNamesEndpoint),
        Route('/ghosts/<key>/', GhostNameEndpoint),
    ]),
])

def main():
    app.run()

if __name__ == '__main__':
    main()
