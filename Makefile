deploy: run-tests
	cd frontend && yarn build
	mkdir website
	mv frontend/dist/* website/
	gcloud app deploy
	rm -f website/*
	rmdir website

run-tests:
	python runner.py ~/google-cloud-sdk --test-path .
